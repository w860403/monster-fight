# monster-fight

【練習】【2016-07-19】 

撰寫簡易水果機，使用HTML、CSS、JavaScript以及jQuery
練習

1.`setTimeout`

2.`Math.random()`

3.建構子及物件

預覽圖如下：
![](https://gitlab.com/w860403/monster-fight/raw/master/doc/monster-fight.PNG)

神奇寶貝建構子
```javascript
function Monster(name, hp, skill) {
    this.name = name;
    this.hp = hp;
    this.skill = skill;
}
```
建立神奇寶貝物件
```javascript
var m1=new Monster("妙花種子",100,"飛葉快刀：10,能量鞭打：25,種子機關槍：35");
var m2=new Monster("小火龍",100,"火花：15,噴射火焰：25,火焰漩渦：20");
var m3=new Monster("傑尼龜",100,"泡泡：20,縮殼：20,水槍：30");
var m4=new Monster("皮卡丘",100,"十萬伏特：10,高壓電擊：40,鋼鐵尾巴：20");
var m5=new Monster("胖丁",100,"唱歌：5,連環巴掌：15,捨身攻擊：50");

```